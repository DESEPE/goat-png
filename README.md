### Created by Daniel Cruzado Poveda  
  
#### About this project  
GoatPNG is a web project oriented to the creation of a website that lets you upload images (similar service as  
imgur and so, if you want a rough comparison). The objective is to learn the use of PHP and SQL. As a  
second objective (not yet in progress), it aims to learn about making a more visual frontend. This project **has  
been developed on Windows environments**, some annotations will be oriented for this OS.
  
### Dependencies and important notes  

It's necessary to have installed a server for the deployment of this project. You have different options, like  
WAMP, XAMP, etc. In my case, I used *WAMP*. 
  
Before you clone the project:  
 - It's still a wip project, is not ready to be deployed (and it mustn't be put on production).   
 - The project will have many possible changes, so some things may get obsolete fastly.  
 - I won't be responsible of the bad use you give to the project or code. The project is given as it, included the possible  
fails or universe destructions implied (maybe not that last thing...).  
 - **I can't say how much time will take certain changes and updates.**  I work at my own path, and the project is aimed for  
 learning, so there is no real objective or deadline on it. With this I want to clarify that **the project can be abandoned AT ANY TIME**.
  
You'll need to create in your "connections" folder a PHP file called db_connections.php  
This file should contain the connection to your database. Here you have an example:  
  
<?php  
  
    $servername = "YOUR_SERVER_NAME";  
    // Use environment variables for the user and pass, for security reasons (unless  
    // you plan to use this project with a local database, where you can be a bit less worried)  
    $username = "USER_OF_YOUR_DB";  
    $password = "PASSWORD_OF_YOUR_USER";  
    $dbname = "NAME_OF_YOUR_DATABASE";  
    // Use port only in case that you are working locally  
    $port = "3308";  
      
    // Create the connection  
    $conn = mysqli_connect($servername, $username, $password, $dbname, $port);  
      
    // Check connection  
    if (!$conn) {  
      // if it fails connecting, we kill the attempt  
        die("Connection failed: " . mysqli_connect_error());  
      }  
  
?>  

The schema of the database is included in the folder "db-schema", in a .zip file (extract it to get the .sql file).  

Note: once created the connection, you'll need to access to your DB through the variable "$conn" if you used the template 
(or whatever name you decided to use).  
  
Also you'll need to create a folder for your image uploads. I called mine "post_uploads"  

For the dependencies, you'll need **Randomlib** and a dependencies manager. I use **Composer** (at the end of this document you'll)  
find the links to their respective pages)

### Credits

Created by Daniel Cruzado Poveda.

Contact me through LinkedIn: https://www.linkedin.com/in/daniel-cruzado-poveda-2824191a5/ (Spanish)  

Composer Library: https://getcomposer.org/  
Randomlib repository: https://github.com/ircmaxell/RandomLib  
