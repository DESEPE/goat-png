<?php

require('connections/db_connect.php');
require('connections/session_checking.php');

check_user_accesibility();

// Once the form is completed, the php script launch this section, that registers the user.
if($_POST && isset($_POST['sendprofileupdate'], $_POST['username'])){
    if ($_POST['username'] != ''){
        if(!checkExistingUser($conn, $_POST['username'])){
            $username = $_POST['username'];
            $query = ("UPDATE user SET nickname=? where id=?;");
            $stmt = mysqli_prepare($conn, $query);
            mysqli_stmt_bind_param($stmt,"ss", $username, $_SESSION['user_id_goatpng']);
            mysqli_stmt_execute($stmt);
            $_SESSION['username_goatpng'] = $username;
            mysqli_stmt_close($stmt);
            echo ('<h3 class="border mt-3" style="color:#2F9C4C;text-align:center;margin:auto;width:50%">
            Your username has been changed succesfully!</h3><br>');

        } else {
            echo ('<h3 class="border mt-3" style="color:#990000;text-align:center;margin:auto;width:50%">
            The user that you specified exists already. Choose a different one</h3><br>');
        }
    }
}

// Function that checks if the database already have an user with the username specified in parameter.
function checkExistingUser($conn, $username){
    $response = false;
    $query = "SELECT nickname from USER where nickname=?";

    $stmt = mysqli_prepare($conn, $query);

    if(!$stmt){
        echo '<script>alert("Error during the execution...")</script>';
    } else {
        mysqli_stmt_bind_param($stmt,"s", $username);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $user_result);
        mysqli_stmt_fetch($stmt);
    }

    if ($user_result == $username){
        $response = true;
    }

    return $response;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="style/profile_style.css">
    <title>Profile - GoatPNG</title>
</head>
<h1> My profile </h1>
<br>

<div class='mx-auto border profile-form text-center'>
    <form method="post" action="<?PHP echo htmlspecialchars($_SERVER['PHP_SELF']); ?>"
        accept-charset="UTF-8">
        <div class="form-group">
        <label for="username">Change your username*</label>
        <br>
        <input type="text" name="username" id="username" placeholder="Username"
        value = "<?PHP echo $_SESSION['username_goatpng'] ?>" 
        aria-describedby="helpId" style="width:30%" required>
        <div id=errorUser>
        </div>
        <small id="helpUsername" class="text-muted">Introduce a new username if you want to change it</small>
        <br>
        <input type="submit" name="sendprofileupdate" class="btn btn-success mx-auto mb"></input>
    </form>
        <br>
        <br>
        <a class= "btn-lg btn-secondary mb-5" href="change_pass.php"> Change my password </a>
        <br>
        <br>
        <a class="btn-lg btn-primary mb-5" href="menu.php"> Back to the menu</a>
</div>

<body>
    
</body>
</html>