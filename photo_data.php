<!DOCTYPE html>

<?php

  include('connections/db_connect.php');
  include('connections/session_checking.php');

  check_user_accesibility();

  if($_GET && isset($_GET['delete_photo']) && isset($_GET['selected_photo'])){

    $query = "DELETE FROM image WHERE location=?";

    $stmt = mysqli_prepare($conn, $query);

    if(!$stmt){
      echo '<script>alert("Error during the execution...")</script>';
    } else {
        mysqli_stmt_bind_param($stmt,"s", $_GET['selected_photo']);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
        unlink($_GET['selected_photo']);

        header("Location: gallery");
        die();
    }
  }
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/gallery_style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Photo - GoatPNG</title>
</head>
<body>
    <div class="img-block">
        <h3><?php echo $_GET['title'] ?><br></h3>
        <a href="<?php echo $_GET['selected_photo']; ?>">
        <img class="image" src="<?php echo $_GET['selected_photo']; ?>"/>
        </a>
    <form method="get" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>"
    onsubmit ="return confirm('Are you sure you want to delete this image? Once done, it will be erased forever!');">
        </br>
        <input type="text" hidden name="selected_photo" value="<?php echo $_GET['selected_photo']?>">
        <input type="submit" class="btn-lg btn-danger" name="delete_photo" value="Delete photo"/>
    </form>
        <br>
        <a href="gallery" class="btn-lg btn-primary">Back to the gallery</a>
        <br>
    </div>

</body>
</html>