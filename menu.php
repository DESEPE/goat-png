<?php

  include('connections/db_connect.php');
  include('connections/session_checking.php');

  check_user_accesibility();

  if(array_key_exists('button-logout', $_POST)) { 
    end_session(); 
  }

?>

<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<link rel="stylesheet" href="style/menu_style.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Menu - GoatPNG</title>
</head>
<body>

<div class="split left">
  <div class="centered">
    <a href="upload-files.php">
    <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.searchpng.com%2Fwp-content%2Fuploads%2F2019%2F03%2FUpload-Icon-PNG-Image-1024x1024.png&f=1&nofb=1"
    style="height=75%;width:75%;"/>
    </a>
    <br>
    <a href="upload-files.php">
    <h2>Upload an image</h2>
    </a>
  </div>
</div>

<div class="split right">
  <div class="centered">
    <a href="gallery.php">
    <img src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fcdn.onlinewebfonts.com%2Fsvg%2Fimg_249682.png&f=1&nofb=1"
    style="height=70%;width:70%;" />
    </a>
    <br>
    <br>
    <a href="gallery.php"><h2>To your gallery</h2></a>
  </div>

<div class="row float-right">
  <form method="post" class="text-right">
    <a type="button" name="button1" class="btn-lg btn-primary" href="profile.php">
      <i class="fa fa-user-o fa-2x" aria-hidden="true"></i>
    </a> 
    <button type="submit" name="button-logout" class="btn-lg btn-danger">
      <i class="fa fa-sign-out fa-2x" aria-hidden="true"></i>
    </button>
  </form> 
</div>
</div>

     
</body>
</html> 
</html>