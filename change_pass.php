<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="style/profile_style.css">
    <title>Change your password - GoatPNG</title>
</head>
<body>

    <h1> Change my password </h1>
    <br>

    <div id="profile-box">
    <form method="post" action="<?PHP echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" 
    accept-charset="UTF-8">
    <div class="profile-form border">
        <label for="old_password">Actual password*</label>
        <input type="old_password" name="old_password" id="old_password"  class="form-control offset-md-5" placeholder="Actual password"
        aria-describedby="helpId" style="width:30%" >
        <small id="helpPassword" class="text-muted">Introduce your old password</small>
        <br>
        <label for="password">Password*</label>
        <input type="password" name="password" id="password"  class="form-control offset-md-5" placeholder="Password"
        aria-describedby="helpId" style="width:30%" >
        <small id="helpPassword" class="text-muted">Introduce your new password</small>
        <label for="password">Repeat the new password*</label>
        <br>
        <input type="password" name="rep_password" id="rep_password"  class="form-control offset-md-5" placeholder="Password"
        aria-describedby="helpId" style="width:30%" >
        <small id="helpPassword" class="text-muted">Introduce the new password again here</small>
        <small id="helpstar" class="text-muted">The marked elements with * are obligatory</small>
        <br>
        <input type="submit" name="sendprofileupdate" class="btn btn-success mx-auto mb"></input>
    </form>
        <br>
        <br>
        <a class= "btn btn-secondary mb-5" href="profile.php"> Back to my profile </a>
        <a class="btn btn-primary mb-5" href="menu.php"> Back to the menu</a>
    </div>
    </div>

</body>
</html>
    