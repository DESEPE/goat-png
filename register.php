<?php

require('connections/db_connect.php');
require('connections/session_checking.php');
// You have to put your own dependencies directory here
require __DIR__ . '/dependencies/vendor/autoload.php';

check_user_is_logged();

// Once the form is completed, the php script launch this section, that registers the user.
if($_POST && isset($_POST['sendregister'], $_POST['username'], $_POST['password'], $_POST['rep_password'])){
    if ($_POST['username'] != '' && $_POST['password'] != '' && $_POST['rep_password'] != ''){

        if ($_POST['password'] == $_POST['rep_password']){
            if(!checkExistingUser($conn, $_POST['username'])){
                $username = $_POST['username'];
                $password = $_POST['password'];

                $query = ("INSERT INTO user(id,nickname,password, extra) VALUES ('NULL',?,?,?);");

                $stmt = mysqli_prepare($conn, $query);

                if(!$stmt){
                    echo '<script>alert("Error during the execution...")</script>';
                } else {

                    $factory = new RandomLib\Factory;
                    $generator = $factory->getMediumStrengthGenerator();
                    $extra = $generator->generateString(32);
                    $hash = crypt($password, '$5$rounds=7000$'. $extra .'$');

                    mysqli_stmt_bind_param($stmt,"sss", $username, $hash, $extra);
                    mysqli_stmt_execute($stmt);
                    echo '<script>alert("The user has been created succesfully!")</script>';
                    mysqli_stmt_close($stmt);
                    header("Location: index.php");
                }
            } else {
                echo ('<span style="color:red;text-align:center;">The user that you specified exists already. 
                Choose a different one</span>');
            }
        } else {
            echo '<span style="color:red;text-align:center;">The passwords are not coincident</span>'; 
        }
    }
}

// Function that checks if the database already have an user with the username specified in parameter.
function checkExistingUser($conn, $username){
    $response = false;
    $query = "SELECT nickname from USER where nickname=?";

    $stmt = mysqli_prepare($conn, $query);

    if(!$stmt){
        echo '<script>alert("Error during the execution...")</script>';
    } else {
        mysqli_stmt_bind_param($stmt,"s", $username);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $user_result);
        mysqli_stmt_fetch($stmt);
    }

    if ($user_result == $username){
        $response = true;
    }

    return $response;
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Register - GoatPNG</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <link rel="stylesheet" href="style/register_style.css">
    </head>
    <body>
        <br><br>
        <div id=register-box class="container-fluid center-block mx-auto">
            <div class="register-form">
            <form method="post" action="<?PHP echo htmlspecialchars($_SERVER['PHP_SELF']); ?>"
                accept-charset="UTF-8">
                <div class="form-group">
                <label for="username">Username*</label>
                <div id=errorUser>
                </div>
                <input type="text" name="username" id="username"  class="form-control offset-md-5" placeholder="Username"
                value = "<?PHP if(isset($_POST['username'])) echo htmlspecialchars($_POST['username']); ?>" 
                aria-describedby="helpId" style="width:17%" required>
                <small id="helpUsername" class="text-muted">Introduce the username you want to use</small>
                <br> <br>
                <label for="password">Password*</label>
                <div ide=errorPass>
                </div>
                <input type="password" name="password" id="password"  class="form-control offset-md-5" placeholder="Password"
                aria-describedby="helpId" style="width:17%" required>
                <small id="helpPassword" class="text-muted">Introduce your password here</small>
                <br>
                <br>
                <label for="password">Repeat your password*</label>
                <div ide=errorPass>
                </div>
                <input type="password" name="rep_password" id="rep_password"  class="form-control offset-md-5" placeholder="Password"
                aria-describedby="helpId" style="width:17%" required>
                <small id="helpPassword" class="text-muted">Introduce your password again here</small>
                </div>
                <small id="helpstar" class="text-muted">The marked elements with * are obligatory</small>
                <br>
                <br>
                <input type="submit" name="sendregister" class="btn btn-success mx-auto mb"></input>
            </form>
            </div>
            <br>
            <a class="btn btn-primary mb-5" href="index.php"> Back to login</a>
            </div>
        </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

    </body>
</html>