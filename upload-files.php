<?php

include('connections/db_connect.php');
include('connections/session_checking.php');

check_user_accesibility();

// Part of the code is taken from w3s tutorials. 
// Link: https://www.w3schools.com/php/php_file_upload.asp 

// This page right now is a bit primitive in terms of front-end. Is up to changes.

if ($_POST && !empty($_FILES) && isset($_POST['post_title'])) {

    $post_title = $_POST['post_title'];
    // You have to create your own folder for the uploads.
    $target_dir = "../goatpng/post_uploads/";
    $uploadOk = true;

    // We need to generate a new name for the file to avoid collisions
    $temp = explode(".", $_FILES["fileToUpload"]["name"]);
    $newfilename = round(microtime(true)) . '.' . end($temp);
    $target_file = $target_dir . $newfilename;
    
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

    // Check if the image file format is valid
    if(isset($_POST["submit"])) {
      $file_type = mime_content_type($_FILES["fileToUpload"]["tmp_name"]);
      if(!stristr($file_type, 'image')) {
        echo "File is not an image. <br>";
        $uploadOk = 0;
      }
    }
    
    // Check if the file already exists
    if (file_exists($target_file)) {
      echo "Sorry, file already exists. <br>";
      $uploadOk = 0;
    }
    
    // Check the file size
    if ($_FILES["fileToUpload"]["size"] > 100000000) {
      echo "Sorry, your file is too large. The maximum size allowed is 100 MB <br>.";
      $uploadOk = 0;
    }
    
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
      echo "Sorry, the format of your file is not valid <br>.";
      $uploadOk = 0;
    }
    
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
      echo "The file couldn't be uploaded due to the errors marked above. Try again.";
    // if everything is ok, try to upload file
    } else {
      if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded. <br>";

        // After uploading the image into the server, we create the register on the database
        create_post_db($conn, $post_title, $target_file);

      } else {
        echo "Sorry, there was an error uploading your file. <br>";
      }
    }
     
}

// Function that prepares the query to insert the register of the upload of the image
    function create_post_db($conn, $post_title, $target_file){
        $query = ("INSERT INTO image(id, location, title, userId) VALUES ('NULL',?,?,?);");

        $stmt = mysqli_prepare($conn, $query);

        if(!$stmt){
            echo '<script>alert("Error during the execution...")</script>';
        } else {

            mysqli_stmt_bind_param($stmt,"sss", $target_file, $post_title, $_SESSION['user_id_goatpng']);
            mysqli_stmt_execute($stmt);
        }
    }


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Upload your images - GoatPNG</title>
    <link rel="stylesheet" href="style\upload_file_style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body>  
<div id="disclaimer" class="disclaimer bg-dark text-white text-center border"> 
<br>
<h5>The maximum size of files permited is 10 MB</h5>
<p>Accepted <b>formats</b>: <b>PNG, JPG, JPEG </b> and <b>GIF</b></p>
</div>
<br>
<div class='content container mx-auto border upload-box text-center'>
    <form action="<?PHP echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" enctype="multipart/form-data">
    <label for="post_title">Title</label>
    <br>
    <input type="text" name="post_title" id="post_title" required
    value = "<?PHP if(isset($_POST['username'])) echo htmlspecialchars($_POST['post_title']); ?>" 
    aria-describedby="helpId" required style="width:50%">
    <br>
    <small id="helpTitle" class="text-muted">Introduce the title of your post here</small>
    <br> <br>
    <label for="post_title">Select image to upload</label>
    <br>
    <input type="file" name="fileToUpload" id="fileToUpload" required>
    <br>
    <small id="helpTitle" class="text-muted">Select which file you want to upload</small>
    <br> <br>
    <input type="submit" class="btn-lg btn-success" value="Upload Image" name="submit">
    </form>
    <br>
</div>
    <br>
    <div class="text-center">
<a type="button" class="btn-lg btn-primary" href="menu.php">
    Back to menu
</a> 
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script> 
</body>
</html>