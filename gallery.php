
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="style/gallery_style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Gallery - GoatPNG</title>
</head>

<body>
    <h1>My Gallery</h1>
    <br>
    <div class="img-block"><a class = "btn-lg btn-primary mt-5" href="menu">Back to menu</a></div>
    <br>
<?php

    include('connections/db_connect.php');
    include('connections/session_checking.php');

    check_user_accesibility();

    function count_pages($conn, $number_of_records_per_page){
        $query = "SELECT COUNT(*) FROM image WHERE userId=?";

        $stmt = mysqli_prepare($conn, $query);
    
        if(!$stmt){
            echo '<script>alert("Error during the execution...")</script>';
        } else {
            mysqli_stmt_bind_param($stmt,"s", $_SESSION['user_id_goatpng']);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
    
            mysqli_stmt_bind_result($stmt,$count);
    
            mysqli_stmt_fetch($stmt);          
            mysqli_stmt_close($stmt);

            return ceil($count / $number_of_records_per_page);
        }
    }

    if (isset($_GET["actual_page"])){

        if ($_GET["actual_page"] == 1){
            header("Location: gallery");
        } else {
            $actual_page = $_GET["actual_page"];
        }

    } else {
        $actual_page = 1;
    }

    $records_per_page = 5;
    $offset = ($actual_page-1) * $records_per_page;

    $total_pages = count_pages($conn,$records_per_page);

    $query = "SELECT location, title FROM image WHERE userId=? LIMIT ?, ?";

    $stmt = mysqli_prepare($conn, $query);

    if(!$stmt){
        echo '<script>alert("Error during the execution...")</script>';
    } else {
        mysqli_stmt_bind_param($stmt,"sss", $_SESSION['user_id_goatpng'], 
                               $offset, $records_per_page);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt);

        mysqli_stmt_bind_result($stmt,$img_location, $title);

?>

    <div class="img-block">

<?php
        while (mysqli_stmt_fetch($stmt)) {
?> 
        <h3><?php echo $title ?><br></h3>
        <a href="<?php echo 'photo_data?selected_photo='.$img_location.'&title='.$title; ?>">
        <img class="image" src="<?php echo $img_location; ?>" />
        </a>
        <br><br>
<?php

    }
    mysqli_stmt_close($stmt);
}
?>
    </div>

    <ul class="pagination pagination-lg justify-content-center">
    <li class="page-item <?php if($actual_page <= 1){ echo 'disabled'; } ?>">
        <a class="page-link" href="<?php if($actual_page > 1)
        { echo "?actual_page=".($actual_page - 1);} ?>">Previous</a>
    </li>
    <?php
        
        for($i = 0; $i < $total_pages; $i++){
    ?>
        <li class="page-item <?php if($actual_page==$i+1){ echo "active";}?>">
            <a class="page-link" href="?actual_page=<?php echo $i+1; ?>"><?php echo $i+1 ?></a>
        </li>
    <?php
        }
    
    ?>
        <li class="page-item <?php if($actual_page >= $total_pages){ echo 'disabled'; } ?>">
            <a class="page-link" href="<?php if($actual_page < $total_pages)
            { echo "?actual_page=".($actual_page + 1); } ?>">Next</a>
        </li>
  </ul>

</body>

</html>