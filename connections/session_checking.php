<?php

    session_start();

    // Function that sets up the session variables that we'll use
    function setup_session_variables($username, $user_id){
            
        $_SESSION['logged_in_goatpng'] = true;
        $_SESSION['username_goatpng'] = $username;
        $_SESSION['user_id_goatpng'] = $user_id;
      
    }

    // Function used to check if the user is logged.
    function check_user_is_logged(){
        if (isset($_SESSION['logged_in_goatpng'])) {
            if($_SESSION['logged_in_goatpng']) {
                header("Location: menu.php");
            }
        }
    }

    // Function that checks if the user can access to a section
    function check_user_accesibility(){
        if (!isset($_SESSION['logged_in_goatpng']) ||
        !$_SESSION['logged_in_goatpng']) {
        header("Location: index.php");
    }
    }

    // Function that deletes all session variables and logout the user
    function end_session(){
        session_unset();
        header("Location: index.php");
    }


?>