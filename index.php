<?php 
  
  include('connections/db_connect.php');
  include('connections/session_checking.php');

  if(isset($_SESSION['logged_in_goatpng'])){
  check_user_is_logged();
  }

  if($_GET && isset($_GET['sendlogin'], $_GET['username'], $_GET['password'])){
    if ($_GET['username'] != '' && $_GET['password'] != ''){

        $username = $_GET['username'];
        $password = $_GET['password'];

        $query = "SELECT id, extra, password FROM user WHERE nickname=?";

        $stmt = mysqli_prepare($conn, $query);

        if(!$stmt){
          echo '<script>alert("Error during the execution...")</script>';
        } else {
          mysqli_stmt_bind_param($stmt,"s", $username);
          mysqli_stmt_execute($stmt);
          mysqli_stmt_bind_result($stmt,$user_id, $salt, $user_hash);
          mysqli_stmt_fetch($stmt);

          $actual_hash = crypt($password, '$5$rounds=7000$'. $salt .'$');

          if(isset($user_hash) && hash_equals($actual_hash, $user_hash)){
            mysqli_stmt_close($stmt);
            
            setup_session_variables($username, $user_id);

            header("Location: menu.php");
            die();
          } else {
            mysqli_stmt_close($stmt);
            echo '<script>alert("The specified credentials are incorrect, try it again")</script>'; 
          }
        }
    }
  }

?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="style/index_style.css">
    <title>Index - GoatPNG</title>
  </head>
  <body>
    <div class="contains">
      <div id="carousel-login" class="carousel slide first-half" data-ride="carousel" data-interval="3000">

          <!-- The slideshow -->
        <div class="carousel-inner">
          <div class="carousel-item active photo">
            <img src="https://isorepublic.com/wp-content/uploads/2018/11/party-crowd-1100x733.jpg" 
            alt="party-image">
          </div>
          <div class="carousel-item photo">
            <img src="https://legismusic.com/wp-content/uploads/2016/09/background-music-for-videogames.jpg" 
            alt="gaming-vr-image">
          </div>
          <div class="carousel-item photo">
            <img src="https://www.washingtonian.com/wp-content/uploads/2017/06/6-30-17-goat-yoga-congressional-cemetery-1.jpg" 
            alt="goat-image">
          </div>
        </div>
      </div>

        <div id="login-box" class="login-box">
        <!--NOT THE BEST SOLUTION, but I needed to center the form somehow -->
        <br><br><br><br><br><br>
          <div class="container border justify-content-center align-items-center" style="width: 50%">
            <form method="get" action="<?PHP echo htmlspecialchars($_SERVER['PHP_SELF']); ?>"
             accept-charset="UTF-8">
              <div class="form-group mx-auto mt-2">
                <label for="username">Username</label>
                <div id=errorUser>
                </div>
                <input type="text" name="username" id="username" class="form-control" placeholder="Username"
                value = "<?PHP if(isset($_POST['username'])) echo htmlspecialchars($_POST['username']); ?>" 
                aria-describedby="helpId" required>
                <small id="helpUsername" class="text-muted">Introduce your username here</small>
                <br> <br>
                <label for="password">Password</label>
                <div ide=errorPass>
                </div>
                <input type="password" name="password" id="password"  class="form-control" placeholder="Very secret!"
                aria-describedby="helpId" required>
                <small id="helpPassword" class="text-muted">Introduce your password here</small>
              </div>
              <input type="submit" name="sendlogin" class="btn btn-success text-center mb-3"></input>
            </form>
          </div>
          <br>

          <p class="text-center">Not registered yet?</p>
          <a class="btn btn-primary mb-5" href="register.php">Register now!</a>

        </div>
  </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  
  </body>
</html>